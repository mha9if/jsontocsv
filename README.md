
jsonToCSVAPICall arguments
1. API URL
2. Path to output file
3. Append Mode (True/False)
4. Path to files with parameters for api call (optional)

jsonToCSVFromFile arguments
1. Path to input file
2. Path to output file
3. Append Mode (True/False) (optional)

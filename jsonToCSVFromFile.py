#parse json/rest data to csv file
             
import requests
import pandas
import os
import sys
import json
  
# reading the data from the file



def jsonToCSV(data, output_path:str, append = False):  #can import and call method
    if (type(data) == type([1,2,3])): #list of dicts
        d = pandas.DataFrame(data)
    elif (type(data) == type({1:2})): #single dict
        d = pandas.DataFrame(data,index=[0])
    if append:
        d.to_csv(output_path,index=False, mode = 'a',header=not(os.path.exists(output_path))) #append to file with same structure
    else:
     d.to_csv(output_path, index = False, mode= 'w', header=True) #write new file
    print("Success")




if __name__ == "__main__": #args: 1: path to input file, 2 path to output file, 3 append mode (True/False), 
    if len(sys.argv) < 3:
        exit(0)
    with open(sys.argv[1]) as f:
        data = f.read()
        data = json.loads(data)
    print("Data acquired")
    if len(sys.argv) >= 4:
        jsonToCSV(data,sys.argv[2],sys.argv[3])
    else:
        jsonToCSV(data,sys.argv[2])
